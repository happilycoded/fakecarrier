package com.zkhq.android;

/*
 * Hello,Source Code Reader. :)
 *
 * 1.Better UI
 * 2.Icon
 * 3.Better Error/Exception Handling
 * 4.Submit bug to deveoper option (with logcat output if possible)
 * 5.Remove Log messages
 * 6.Move to SDCard option
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FakeCarrier extends Activity {
	
	public ProgressDialog dialog;
	public SharedPreferences myPrefs;
	private ExecShell execShell = new ExecShell();
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button set_btn = (Button)findViewById(R.id.btn_set);
        set_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(execShell.checkIfConfigFileBackupExists())
					execShell.backupConfig();
				else
					Log.d("backup not found", "No backup");
				
				EditText desiredNameTxt = (EditText)findViewById(R.id.hack_name_txt);
				String desiredName = desiredNameTxt.getText().toString();
				hackNow(desiredName);
			}
		});
        Button reset_btn = (Button)findViewById(R.id.btn_reset);
        reset_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				execShell.restoreConfig();
				//TODO
				//show a message " backup restored"
			}
		});
        
        myPrefs = this.getSharedPreferences("myPrefs", MODE_PRIVATE);
        
        if(myPrefs.getBoolean("rooted", false)){
        	//This dialogue is just a fancy one,not needed
	        this.dialog = ProgressDialog.show(FakeCarrier.this, "", "Ninja is checking if we are root. Hold it tight..", true);
	    	this.dialog.show();
	    	
	    	Handler handler = new Handler();
	    	handler.postDelayed(new Runnable() {
				@Override
				public void run() {
		
					SharedPreferences.Editor prefsEditor = myPrefs.edit();
		            prefsEditor.putBoolean("rooted", execShell.isDeviceRooted());
		            prefsEditor.commit();
		            dialog.dismiss(); 
				}
			}, 5000);
        }
        
        initToolBox();
        
    }
        
    void initToolBox(){
    	try
        {
        	if(execShell.isDeviceRooted()){
        		String[] cmd = {""};
        		execShell.runAsRoot(cmd);
        		
        		//IM NOT SURE WHETHER THIS WORKS WITH CDMA
        		//persist.radio.nitz_long_ons_0 is also another option,so far we're good with gsm.sim,let the bugs come and we'll decide
        		
        		InputStream stdOperatorIn = Runtime.getRuntime().exec("getprop gsm.sim.operator.alpha").getInputStream();
        		InputStreamReader stdInputStreamReader = new InputStreamReader(stdOperatorIn);
        		String orig_operator = new BufferedReader(stdInputStreamReader).readLine();
        		
        		//Make UI Changes from here.
        		TextView origTxtView = (TextView)findViewById(R.id.orig_txt);
        		origTxtView.setText("Original Name is : "+orig_operator);

        	}else{
        		AlertDialog exitDialog = new AlertDialog.Builder(FakeCarrier.this)
                .setTitle("Sorry dude,your device is not Rooted.!")
                .setMessage("Remember,we told you earlier?\n" +
                		"This will only work on a rooted phone,\n" +
                		"Screw warranty,root your phone and come back." +
                		"As of now you have NO options,but to Exit.! We're Sorry.!!")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                   	 Intent intent = new Intent(Intent.ACTION_MAIN);
                   	 intent.addCategory(Intent.CATEGORY_HOME);
                   	 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   	 startActivity(intent);
                    }
                }).create();
       		 exitDialog.show();
        	}
         
        }
        catch (IOException localIOException)
        {
          while (true){
        	  localIOException.printStackTrace();
        	  Log.d("Holy shit,there is an Exception :",localIOException.getCause().toString());
          }
            
        }
    }
    void hackNow(String newOperatorName){
		//Telephony Informations.not sure whether to take it from getprop gsm.sim.operator or from the API,better from API as 
    	//2 options,take from getprop gsm.sim.operator options or from telephony API,second one seems to be safe,it might work with cmda too i guess.figure out.
		//TODO
		//Test on CDMA phones,detect the type, add the spn-conf.xml and see if it works
		 TelephonyManager tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		 String origOperatorName = tel.getNetworkOperator().toString();
		 
		 if(tel.getPhoneType()!=TelephonyManager.PHONE_TYPE_GSM){
			 //TODO
			 //Something needs to be done here,figure out later,dont have a CDMA phone now
		 }
		
		 if(execShell.checkIfConfigFileExists()){ 
			 //This is shot in the dark,im not sure wheter it works on all devices
			 //If works,this is by @r3dsm0k3,if not,i dont know who did this. :P
			 execShell.removeOriginalSpnConfig();
		 }
		 execShell.addNewSpnConfig(origOperatorName, newOperatorName);
		 
		 AlertDialog noConfigInPlaceAlert = new AlertDialog.Builder(FakeCarrier.this)
         .setTitle("Do you believe in GOD ?")
         .setMessage("We think everything is proper cool and done,\n" +
         		"You need to restart your device to make sure.\n" +
         		"If it is not working,file a bug.\n" +
         		"Now lets try our luck with Reboot?,\n")
         .setPositiveButton("mmm,yeah", new DialogInterface.OnClickListener() {
             public void onClick(DialogInterface dialog, int whichButton) {
            	execShell.reboot();
             }
         })
         .setNegativeButton("no,later",null).create();
 		noConfigInPlaceAlert.show();
    }

}
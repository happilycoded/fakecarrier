package com.zkhq.android;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.util.Log;

public class ExecShell {

    private static String LOG_TAG 				= ExecShell.class.getName();
    
    private static String config_file 			= "/system/etc/spn-config.xml";
    private static String config_file_backup 	= "/system/etc/spn-config.xml.bkp";
    
    private static String[] cmd_restore_config  = {"cat "+config_file_backup+" > "+config_file,"chmod 644 "+config_file};
    private static String[] cmd_backup_config 	= {"cp "+config_file+" "+config_file_backup};
    private static String[] cmd_remove_config 	= {"rm "+config_file};
    
    public static enum SHELL_CMD {
        check_su_binary(new String[] {"/system/xbin/which","su"}),;
        String[] command;
        SHELL_CMD(String[] command){
            this.command = command;
        }
    }
    
    //TODO
    //Clean the mess,dont ya? ;)
    public ArrayList<String> executeCommand(SHELL_CMD shellCmd){
    	
        String line = null;
        ArrayList<String> fullResponse = new ArrayList<String>();
        Process localProcess = null;
        try {
            localProcess = Runtime.getRuntime().exec(shellCmd.command);
        } catch (Exception e) {
            return null;
            //e.printStackTrace();
        }
       // BufferedWriter out = new BufferedWriter(new OutputStreamWriter(localProcess.getOutputStream()));
        BufferedReader in = new BufferedReader(new InputStreamReader(localProcess.getInputStream()));

        try {
            while ((line = in.readLine()) != null) {
                Log.d(LOG_TAG, "--> Line received: " + line);
                fullResponse.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(LOG_TAG, "--> Full response was: " + fullResponse);

        return fullResponse;
    }
    void runAsRoot(String[] cmds){
     	try{
     		 Process p = Runtime.getRuntime().exec("su");
             DataOutputStream os = new DataOutputStream(p.getOutputStream());  
             os.writeBytes("mount -o rw,remount -t yaffs2 /system\n");
             os.flush();
             for (String tmpCmd : cmds) {
                     os.writeBytes(tmpCmd+"\n");
                     os.flush();
             }           
             //os.writeBytes("exit\n");  
             //os.flush();
     	}
     	catch (IOException e) {
 			e.printStackTrace();
 		}   
     }
     
     //Helper functions
     void removeOriginalSpnConfig(){
		 this.runAsRoot(cmd_remove_config);
     }
     void addNewSpnConfig(String origOperatorName,String newOperatorName){
    	 String xmlcmd = "echo \"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\"?>\n<spnOverrides>\n<spnOverride numeric=\\\""+origOperatorName+"\\\" spn=\\\""+newOperatorName+"\\\"/>\n</spnOverrides>" + "\" > /system/etc/spn-conf.xml";
		 String[] cmd2 = {xmlcmd,"chmod 644 /system/etc/spn-conf.xml"};
		 new ExecShell().runAsRoot(cmd2);
     }
     void reboot(){
    	 String[] cmd3 = {"reboot"};	
    	 new ExecShell().runAsRoot(cmd3);
     } 
     boolean checkIfConfigFileBackupExists(){
     	File file = new File(config_file_backup);
     	return file.exists() ? true : false;
     }
     boolean checkIfConfigFileExists(){
     	File file = new File(config_file);
     	return file.exists() ? true : false;
     } 
     void backupConfig(){
     	this.runAsRoot(cmd_backup_config);
     }
     void restoreConfig(){
     	this.runAsRoot(cmd_restore_config);
     }
     //BETTER  ROOT DETECTION
     public boolean isDeviceRooted() {
     	 if (checkRootMethod1())
     		 return true;
          if (checkRootMethod2())
         	 return true;
          if (checkRootMethod3())
         	 return true;
          return false;
     }
     private  boolean checkRootMethod1(){
         String buildTags = android.os.Build.TAGS;

         if (buildTags != null && buildTags.contains("test-keys")) {
             return true;
         }
         return false;
     }
     private  boolean checkRootMethod2(){
         try {
             File file = new File("/system/app/Superuser.apk");
             if (file.exists()) {
                 return true;
             }
         } catch (Exception e) { }

         return false;
     }
     private  boolean checkRootMethod3() {
         if (new ExecShell().executeCommand(SHELL_CMD.check_su_binary) != null){
             return true;
         }else{
             return false;
         }
     }
}
